# CMake generated Testfile for 
# Source directory: C:/Programming/STE6245/collision_library_interface
# Build directory: C:/Programming/STE6245/debug_interface
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(DSpherePlaneCollisionDetection.Collision "C:/Programming/STE6245/debug_interface/dsphere_plane_detection_tests.exe" "--gtest_filter=DSpherePlaneCollisionDetection.Collision")
add_test(DSpherePlaneCollisionDetection.CollisionCurrTInDt "C:/Programming/STE6245/debug_interface/dsphere_plane_detection_tests.exe" "--gtest_filter=DSpherePlaneCollisionDetection.CollisionCurrTInDt")
add_test(DSpherePlaneCollisionDetection.ParallelNoCollision "C:/Programming/STE6245/debug_interface/dsphere_plane_detection_tests.exe" "--gtest_filter=DSpherePlaneCollisionDetection.ParallelNoCollision")
add_test(DSpherePlaneCollisionDetection.ParallelAndTouching "C:/Programming/STE6245/debug_interface/dsphere_plane_detection_tests.exe" "--gtest_filter=DSpherePlaneCollisionDetection.ParallelAndTouching")
add_test(DSpherePlaneCollisionDetection.CollisionMindTheParent "C:/Programming/STE6245/debug_interface/dsphere_plane_detection_tests.exe" "--gtest_filter=DSpherePlaneCollisionDetection.CollisionMindTheParent")
add_test(DSpherePlaneCollisionDetection.NoCollisionMindTheParent "C:/Programming/STE6245/debug_interface/dsphere_plane_detection_tests.exe" "--gtest_filter=DSpherePlaneCollisionDetection.NoCollisionMindTheParent")
add_test(DSpherePlaneImpactResponse.NewVelocity "C:/Programming/STE6245/debug_interface/dsphere_plane_response_tests.exe" "--gtest_filter=DSpherePlaneImpactResponse.NewVelocity")
add_test(DSphereDSphereCollisionDetection.Collision "C:/Programming/STE6245/debug_interface/dsphere_dsphere_detection_tests.exe" "--gtest_filter=DSphereDSphereCollisionDetection.Collision")
add_test(DSphereDSphereCollisionDetection.CollisionDifferentDt "C:/Programming/STE6245/debug_interface/dsphere_dsphere_detection_tests.exe" "--gtest_filter=DSphereDSphereCollisionDetection.CollisionDifferentDt")
add_test(DSphereDSphereCollisionDetection.CollisionCurrTInDt "C:/Programming/STE6245/debug_interface/dsphere_dsphere_detection_tests.exe" "--gtest_filter=DSphereDSphereCollisionDetection.CollisionCurrTInDt")
add_test(DSphereDSphereCollisionDetection.CollisionCurrTInDtTwo "C:/Programming/STE6245/debug_interface/dsphere_dsphere_detection_tests.exe" "--gtest_filter=DSphereDSphereCollisionDetection.CollisionCurrTInDtTwo")
add_test(DSphereDSphereCollisionDetection.CollisionCurrTInDtThree "C:/Programming/STE6245/debug_interface/dsphere_dsphere_detection_tests.exe" "--gtest_filter=DSphereDSphereCollisionDetection.CollisionCurrTInDtThree")
add_test(DSphereDSphereCollisionDetection.ParallelNoCollision "C:/Programming/STE6245/debug_interface/dsphere_dsphere_detection_tests.exe" "--gtest_filter=DSphereDSphereCollisionDetection.ParallelNoCollision")
add_test(DSphereDSphereCollisionDetection.ParallelAndTouching "C:/Programming/STE6245/debug_interface/dsphere_dsphere_detection_tests.exe" "--gtest_filter=DSphereDSphereCollisionDetection.ParallelAndTouching")
add_test(DSphereDSphereCollisionDetection.CollisionMindTheParents "C:/Programming/STE6245/debug_interface/dsphere_dsphere_detection_tests.exe" "--gtest_filter=DSphereDSphereCollisionDetection.CollisionMindTheParents")
add_test(DSphereDSphereCollisionDetection.NoCollisionMindTheParents "C:/Programming/STE6245/debug_interface/dsphere_dsphere_detection_tests.exe" "--gtest_filter=DSphereDSphereCollisionDetection.NoCollisionMindTheParents")
add_test(DSphereDSphereImpactResponse.NewVelocity "C:/Programming/STE6245/debug_interface/dsphere_dsphere_response_tests.exe" "--gtest_filter=DSphereDSphereImpactResponse.NewVelocity")
