# CMake generated Testfile for 
# Source directory: C:/Programming/STE6245/collision_library_template
# Build directory: C:/Programming/STE6245/template_build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(MyUniqueTestCategory.MyCategory_UniqueTestName_WhichPasses "C:/Programming/STE6245/template_build/hello_world.exe" "--gtest_filter=MyUniqueTestCategory.MyCategory_UniqueTestName_WhichPasses")
add_test(MyUniqueTestCategory.MyCategory_UniqueTestName_WhichFails "C:/Programming/STE6245/template_build/hello_world.exe" "--gtest_filter=MyUniqueTestCategory.MyCategory_UniqueTestName_WhichFails")
