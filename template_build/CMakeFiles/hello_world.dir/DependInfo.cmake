# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "C:/Programming/STE6245/collision_library_template/unittests/hello_world.cc" "C:/Programming/STE6245/template_build/CMakeFiles/hello_world.dir/unittests/hello_world.cc.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GM_GL_DEBUG"
  "GM_STREAM"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "C:/Programming/STE6245/build/gmlib/include"
  "C:/Program Files (x86)/glew/include"
  "C:/Programming/STE6245/collision_library_template/../collision_library_interface/include"
  "C:/Programming/STE6245/collision_library_template/include"
  "C:/Program Files (x86)/googletest-distribution/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "C:/Programming/STE6245/template_build/CMakeFiles/collision_library_stub.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
